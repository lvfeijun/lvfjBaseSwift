//
//  UHomeViewController.swift
//  SwiftUI
//
//  Created by lvfeijun on 2021/12/27.
//  Copyright © 2021 lvfeijun. All rights reserved.
//

import UIKit

class UHomeViewController: UPageViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func configNavigationBar() {
        super.configNavigationBar()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_search"), target: self, action: #selector(selectAction))
    }
    
    @objc private func selectAction() {
        navigationController?.pushViewController(USearchViewController(), animated: true)
    }
}
