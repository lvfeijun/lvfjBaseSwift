//
//  UMineViewController.swift
//  SwiftUI
//
//  Created by lvfeijun on 2022/1/5.
//  Copyright © 2022 lvfeijun. All rights reserved.
//

import UIKit

//MARK:  定义枚举
public enum MineType: Alignment {
    case MineTypeLeft
    case MineTypeJustified
    case MineTypeRight
}

class UMineViewController: UBaseViewController {
    
    //MARK:  私有 懒加载 变量 数组
    private lazy var myArray: Array = {
        return [[["icon":"mine_vip", "title": "我的VIP"],
                 ["icon":"mine_coin", "title": "充值妖气币"]],
            
                [["icon":"mine_accout", "title": "消费记录"],
                 ["icon":"mine_subscript", "title": "我的订阅"],
                 ["icon":"mine_seal", "title": "我的封印图"]],
                
                [["icon":"mine_message", "title": "我的消息/优惠券"],
                 ["icon":"mine_cashew", "title": "妖果商城"],
                 ["icon":"mine_freed", "title": "在线阅读免流量"]],
                
                [["icon":"mine_feedBack", "title": "帮助中心"],
                 ["icon":"mine_mail", "title": "我要反馈"],
                 ["icon":"mine_judge", "title": "给我们评分"],
                 ["icon":"mine_author", "title": "成为作者"],
                 ["icon":"mine_setting", "title": "设置"]]]
    }()
    
    private lazy var head: UMineHead = {
        return UMineHead(frame: CGRect(x: 0, y: 0, width: UScreenWidth, height: 200))
    }()
    
    //MARK:  定义属性  ={  return }()
    private lazy var navigationBarY: CGFloat = {
        return navigationController?.navigationBar.frame.maxY ?? 0
    }()
    
    lazy var tableView: UITableView = {
        let tw = UITableView(frame: .zero, style: .grouped)
        tw.backgroundColor = UIColor.background
        tw.delegate = self
        tw.dataSource = self
        tw.register(cellType: UBaseTableViewCell.self)
        //MARK:  弱引用 防止被释放导致闪退
        tw.uHead = URefreshHeader{ [weak self] in self?.loadData() }
        tw.uempty = UEmptyView{ [weak self] in self?.loadData() }
        return tw
    }()
    
    /*
     class 我的类: NSObject {
        func 打招呼(名字: String) {
            print("哈喽，\(名字)")
        }
    }
    我的类().打招呼("小明")
     
     @objc 也可以重新声明方法或者变量的名字
     
     @objc(MyClass)
         class 我的类 {
             @objc(greeting:)
             func 打招呼(名字: String) {
                 print("哈喽，\(名字)")
             }
         }
     */
    //MARK: @objc暴露给Objective-C的类、方法、属性等
    @objc private func loadData(){
//        ApiLoadingProvider.request(UApi.rankList, model: RankinglistModel.self) { (returnData) in
        //MARK: returnData 为接口返回的参数
//            self.tableView.uHead?.endRefreshing()
//            self.tableView.uempty?.allowShow = true
//
//            self.rankList = returnData?.rankinglist ?? []
//            self.tableView.reloadData()
//        }
    }
    
    //MARK: 匿名函数
    /*
     创建函数体，包括花括号，但是不需要函数声明
     如果必要，将函数的参数列表与返回类型作为花括号中的第一行，后跟关键字in
     */
    /*
     一、无参数无返回值的函数
     func whatToAnimate(){
         self.myButton.frame.origin.y += 20
     }
     无参数无返回值的匿名函数
     {
         () -> () in
         self.myButton.frame.origin.y += 20
     }
     
     二、有参数无返回值的函数
     func whatToDoLater(finished: Bool){
         print("finished: \(finished)")
     }
     有参数无返回值的匿名函数
     {
         (finished: Bool) -> () in
         print("finished: \(finished)")
     }
     
     可省略的地方：
     省略返回类型
     没有参数可以省略 in 这一样
     省略参数类型
     省略圆括号，这个是如果就一个参数，并且我们编译器可以推断出其类型的话
     省略 in， 直接用名字比如 $0
     省略参数名，用_代替
     省略函数实参标签（又叫尾函数
     ...
     一路省略下来，匿名函数真是节约。
     */
    
    //MARK:  解闭包
    //闭包可以捕获和存储其所在上下文中任意常量和变量的引用。 这就是所谓的闭合并包裹着这些常量和变量，俗称闭包。
    /*
     reversed = sorted(names, { (s1: String, s2: String) -> Bool in
         return s1 > s2
     })
     利用省略到：
     reversed = sorted(names, >)
     再利用 trailling closure的特性
     reversed = sorted(names) { $0 > $1 }
     */
    
    //MARK: @escaping  Escaping Closure（逃逸闭包  当函数执行后该闭包仍然处在，相当于block，用于处理一些延时操作
    //如果一个闭包被作为一个参数传递给一个函数，并且在函数return之后才被唤起执行，那么这个闭包是逃逸闭包。
//    func moreActionClosure(_ closure: @escaping USearchTHeadMoreActionClosure) {
//        moreActionClosure = closure
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .top
    }
    
    override func configUI() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints{
            $0.edges.equalTo(self.view.usnp.edges).priority(.low)
            $0.top.equalToSuperview()
        }
        
        tableView.parallaxHeader.view = head
        tableView.parallaxHeader.height = 200
        tableView.parallaxHeader.minimumHeight = navigationBarY
        tableView.parallaxHeader.mode = .fill
    }
    
    override func configNavigationBar() {
        super.configNavigationBar()
        navigationController?.barStyle(.clear)
        tableView.contentOffset = CGPoint(x: 0, y: -tableView.parallaxHeader.height)
    }
}

//MARK: 协议都写在 extension里，不实现必要协议会报错
extension UMineViewController: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: _  表示忽略
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= -(scrollView.parallaxHeader.minimumHeight) {
            navigationController?.barStyle(.theme)
            navigationItem.title = "我的"
        } else {
            navigationController?.barStyle(.clear)
            navigationItem.title = ""
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionArrary = myArray[section]
        return sectionArrary.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: UBaseTableViewCell.self)
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .default
        let seciotnArray = myArray[indexPath.section]
        //MARK:  定义的时候，不指定类型，系统会自动推到你的类型，最好还是写
        let dict: [String : String] = seciotnArray[indexPath.row]
        cell.imageView?.image = UIImage(named: dict["icon"] ?? "")
        cell.textLabel?.text = dict["title"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
}
