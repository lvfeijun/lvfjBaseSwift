//
//  USearchTHead.swift
//  SwiftUI
//
//  Created by lvfeijun on 2022/1/5.
//  Copyright © 2022 lvfeijun. All rights reserved.
//

import UIKit

typealias USearchTHeadMoreActionClosure = ()->Void

protocol USearchTHeadDelegate: class {
    func searchTHead(_ searchTHead:USearchTHead, moreAction button: UIButton)
}


class USearchTHead: UBaseTableViewHeaderFooterView {
    
    weak var delegate: USearchTHeadDelegate?
    
    private var moreActionClosure: USearchTHeadMoreActionClosure?
    
    lazy var titleLabel: UILabel = {
       let tl = UILabel()
        tl.font = UIFont.systemFont(ofSize: 14)
        tl.textColor = UIColor.gray
        return tl
    }()
    
    lazy var moreButton: UIButton = {
        let mn = UIButton(type: .custom)
        mn.setTitleColor(UIColor.lightGray, for: .normal)
        mn.addTarget(self, action: #selector(moreAction), for: .touchUpInside)
        return mn
    }()
    
    @objc private func moreAction(button: UIButton){
        delegate?.searchTHead(self, moreAction: button)
        
        guard let closure = moreActionClosure else { return }
        closure()
    }
    
    
    func moreActionClosure(_ closure: @escaping USearchTHeadMoreActionClosure) {
        moreActionClosure = closure
    }
}
