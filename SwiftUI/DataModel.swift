//
//  DataModel.swift
//  SwiftUI
//
//  Created by lvfeijun on 2019/8/6.
//  Copyright © 2019年 lvfeijun. All rights reserved.
//

import UIKit

//class DataModel:NSObject,NSCoder {

class DataModel:Codable {
    
    var label:String
    var textField:String
    var textView:String
    
    init(label:String,textField:String,textView:String) {
        self.label = label
        self.textField = textField
        self.textView = textView
    }
    
//    func encode(with aCoder: NSCoder) {
//        aCoder.encode(lalbe, forKey: "lalbe")
//        aCoder.encode(textField, forKey: "textField")
//        aCoder.encode(textView, forKey: "textView")
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        lalbe = aDecoder.decodeObject(forKey: "lalbe") as! String
//        textField = aDecoder.decodeObject(forKey: "textField") as! String
//        textView = aDecoder.decodeObject(forKey: "textView") as! String
//    }
    
}
