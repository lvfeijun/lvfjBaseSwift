//
//  ViewController.swift
//  SwiftUI
//
//  Created by lvfeijun on 2019/8/3.
//  Copyright © 2019年 lvfeijun. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var datas:[String] = ["1","2"];
    var tableView:UITableView = UITableView()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identify = "identify"
        var cell = tableView.dequeueReusableCell(withIdentifier: identify)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: identify)
        }
        cell?.textLabel?.text = datas[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var ctl:UIViewController
        
        switch indexPath.row {
        
        case 1:
            ctl = TableViewController()
            break;
            
        default:
            ctl = FirstViewController()
        }
        self.navigationController?.pushViewController(ctl, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = UITableView.init(frame: view.frame, style: UITableView.Style.plain)
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
    }
}


