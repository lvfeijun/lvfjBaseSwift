//
//  StringExtension.swift
//  SwiftUI
//
//  Created by lvfeijun on 2021/12/28.
//  Copyright © 2021 lvfeijun. All rights reserved.
//

import Foundation

extension String {
    public func substring(form index:Int) -> String {
        if self.count > index {
            let startInde = self.index(self.startIndex, offsetBy: index)
            let subString = self[startIndex..<self.endIndex]
            return String(subString)
        }else{
            return self
        }
    }
}
