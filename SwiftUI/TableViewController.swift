//
//  TableViewController.swift
//  SwiftUI
//
//  Created by lvfeijun on 2019/8/5.
//  Copyright © 2019年 lvfeijun. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    public var data:[String] = ["1","2","3","4"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "tableView"
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem.init(title: "保存", style: UIBarButtonItem.Style.done, target: self, action: #selector(save))
        self.tableView.backgroundColor = UIColor.white;
        self.tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.data.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identify = "identify"
//        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        var cell = tableView.dequeueReusableCell(withIdentifier: identify)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: identify)
        }
//        var text:String = self.data[indexPath.row] ?? ""
//        print(text)
        cell?.textLabel?.text = self.data[indexPath.row]
        // Configure the cell...

        return cell!
    }
    
    @objc func save()  {
        //        UserDefaults.standard.set(self.data, forKey: "data")
        
        if self.data.count<1 {
            return
        }
        let model:DataModel = DataModel(label: self.data[0], textField: self.data[1], textView: self.data[2])
        do{
            let data = try PropertyListEncoder().encode(model)
            let filePath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/data.dat"
            if FileManager.default.fileExists(atPath: filePath) == false{
                do{
                    try FileManager.default.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                }catch{}
            }
            if NSKeyedArchiver.archiveRootObject(data, toFile: filePath+"text.text"){
                print("OK")
            }else{
                print("Fail")
            }
        }catch{
            print("try 失败")
        }

    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
